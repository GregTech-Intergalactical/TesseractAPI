package tesseract.api.gt;

import net.minecraft.nbt.CompoundTag;
import net.minecraftforge.common.util.INBTSerializable;

public interface IEnergyHandler extends IGTNode, INBTSerializable<CompoundTag> {

}
